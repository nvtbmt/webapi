﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [EnableCors("*", "*", "*")]
    public class FacultyController : ApiController
    {
        StudentManagementEntities dbcontext = new StudentManagementEntities();

        // GET api/Faculty
        public IHttpActionResult Get()
        {
            var faculties = (from f in dbcontext.Faculties
                            select new FacultyModel
                            {
                                Id = f.Id,
                                FacultyName = f.FacultyName,
                                TrainingTime = f.TrainingTime
                            }).ToList();

            if (faculties.Count() > 0)
                return Ok(faculties);
            return NotFound();
        }

        // GET api/Faculty/5
        public IHttpActionResult Get(Guid id)
        {
            FacultyModel faculty = (from f in dbcontext.Faculties
                                    where f.Id == id
                                    select new FacultyModel
                                    {
                                        Id = f.Id,
                                        FacultyName = f.FacultyName,
                                        TrainingTime = f.TrainingTime
                                    }).FirstOrDefault();

            if (faculty != null)
                return Ok(faculty);
            return NotFound();
        }
    }
}
