﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    //[EnableCors(origins: "https://localhost:44397", headers: "*", methods: "*")]
    //[EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    [EnableCors("*", "*", "*")]
    public class StudentController : ApiController
    {
        StudentManagementEntities dbcontext = new StudentManagementEntities();

        // GET api/<controller>
        public IHttpActionResult Get()
        {
            var students = (from s in dbcontext.Students
                            from f in dbcontext.Faculties
                            where s.FacultyId == f.Id
                   select new StudentModel
                   {
                       Id = s.Id,
                       FirstName = s.FirstName,
                       LastName = s.LastName,
                       Gender = s.Gender,
                       FacultyId = s.FacultyId,
                       FacultyName = f.FacultyName,
                       EnrollDate = s.EnrollDate,
                       FinishDate = s.FinishDate
                   }).ToList();

            if (students.Count() > 0)
                return Ok(students);
            return NotFound();
        }

        // GET api/<controller>/5
        public IHttpActionResult Get(Guid id)
        {
            StudentModel student = (from s in dbcontext.Students
                                    from f in dbcontext.Faculties
                                    where s.FacultyId == f.Id
                   where s.Id == id
                   select new StudentModel {
                       Id = s.Id,
                       FirstName = s.FirstName,
                       LastName = s.LastName,
                       Gender = s.Gender,
                       FacultyId = s.FacultyId,
                       FacultyName = f.FacultyName,
                       EnrollDate = s.EnrollDate,
                       FinishDate = s.FinishDate
                   }).FirstOrDefault();

            if (student != null)
                return Ok(student);
            return NotFound();
        }

        // POST api/<controller>
        public IHttpActionResult Post([FromBody] Student student)
        {
            dbcontext.Students.Add(new Student()
            {
                //Id = Guid.NewGuid(),
                FirstName = student.FirstName,
                LastName = student.LastName,
                Gender = student.Gender,
                FacultyId = student.FacultyId
            });

            dbcontext.SaveChanges();

            return Ok();
        }

        // PUT api/<controller>/5
        public IHttpActionResult Put([FromBody] Student student)
        {
            Student ThisStudent = dbcontext.Students.Where(s => s.Id == student.Id).FirstOrDefault();
            if (ThisStudent != null)
            {
                ThisStudent.FirstName = student.FirstName;
                ThisStudent.LastName = student.LastName;
                ThisStudent.Gender = student.Gender;
                ThisStudent.FacultyId = student.FacultyId;

                dbcontext.SaveChanges();
            }
            else
                return NotFound();

            return Ok();
        }

        // DELETE api/<controller>/5
        public IHttpActionResult Delete(Guid id)
        {
            Student ThisStudent = dbcontext.Students.Where(s => s.Id == id).FirstOrDefault();
            if (ThisStudent != null)
            {
                dbcontext.Entry(ThisStudent).State = System.Data.Entity.EntityState.Deleted;
                dbcontext.SaveChanges();
            }
            else
                return NotFound();

            return Ok();

        }

        // DELETE api/<controller>/5
        public IHttpActionResult Delete([FromUri] Guid[] ids)
        {
            Student[] ThisStudents = dbcontext.Students.Where(s => ids.Contains(s.Id)).ToArray();
            if (ThisStudents != null && ThisStudents.Length > 0)
            {
                for (int i = 0; i < ThisStudents.Length; i++)
                {
                    dbcontext.Entry(ThisStudents[i]).State = System.Data.Entity.EntityState.Deleted;
                }
                
                dbcontext.SaveChanges();
            }
            else
                return NotFound();

            return Ok();

        }

        [HttpGet]
        public IHttpActionResult getStatistics()
        {
            var statistics = (from s in dbcontext.Students
                             group s by s.EnrollDate.Year into g
                             select new StudentStatisticsModel
                             {
                                 year = g.Key,
                                 total = g.Count(),
                                 outOfDateTotal = g.Where(x => x.FinishDate.Value < DateTime.Now).Count()
                             }).ToList();
            return Ok(statistics);
        }
    }
}