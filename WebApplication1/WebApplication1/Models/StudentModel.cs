﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class StudentModel
    {
        public System.Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Gender { get; set; }
        public System.Guid FacultyId { get; set; }
        public System.DateTime EnrollDate { get; set; }
        public Nullable<System.DateTime> FinishDate { get; set; }
    }
}