﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class FacultyModel
    {
        public System.Guid Id { get; set; }
        public string FacultyName { get; set; }
        public Nullable<double> TrainingTime { get; set; }
    }
}