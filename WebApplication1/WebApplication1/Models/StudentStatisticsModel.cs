﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class StudentStatisticsModel
    {
        public int year { get; set; }
        public int total { get; set; }
        public int outOfDateTotal { get; set; }
    }
}